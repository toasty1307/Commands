﻿namespace CommandsTest
{
    public struct Config
    {
        public string Token { get; set; }
        public string Prefix { get; set; }
    }
}